package bibliobus;
import java.util.*;

public class Bibliobus {
	private String nom;
	private int taille = 100;
	private int limite = 0;
	Media[] catalogue = new Media[taille];

	public Bibliobus(String nom) {
		this.nom = nom;
	}

	public Bibliobus(String nom, int taille) {
		this.nom = nom;
		this.taille = taille;
	}

	public void ajoutLivre(String titre, String auteur, int nbExemplaire, Egenre genre,String editeur) {
		Livre l = new Livre(titre.toUpperCase(), auteur.toUpperCase(), editeur, genre, 1 );
		if (limite < taille) {
			for (int i = 0; i < limite; i++) {
				if (catalogue[i].equals(l)) {
					catalogue[i].nouvelExemplaire();
					return;
				}
			}
			catalogue[limite] = l;
			limite++;
		} else {
			System.out.println("plus de place");
		}
	}

	public void ajoutLivre(String titre, String auteur, Egenre genre, String editeur) {
		Livre l = new Livre(titre, auteur, editeur, genre, 0);
		if (limite < taille) {
			catalogue[limite] = l;
			limite++;
		} else {
			System.out.println("plus de place");
		}
	}

	public void afficheCatalogue() {
		for (int i = 0; i < limite; i++) {
			System.out.println(catalogue[i]);
		}
	}

	public String getAuteur(int id) {
		return catalogue[id].getAuteur();
		// Si catalogue est une liste : return catalogue.get(id).getAuteur();
	}
	
	public String getTitre(int id) {
		return catalogue[id].getTitre();
	}

	public String getEditeur(int id) {
		return ((Livre) catalogue[id]).getEditeur();
	}

	public int getNbExemp(int id) {
		return catalogue[id].getNbExemp();
	}

	public Egenre getGenre(int id) {
		return catalogue[id].getGenre();
	}

	public void afficheLivre(int id) {
		String ch = "[ " + id + " ] " + this.getTitre(id) + " " + this.getAuteur(id) + " " + this.getEditeur(id) + " "
				+ this.getGenre(id) + " . Nombre exemplaires : " + this.getNbExemp(id);
		System.out.println(ch);
	}

	public void afficheListLivrePresent() {
		for (int i = 0; i < limite; i++) {
			if (catalogue[i].estPresent()) {
				afficheLivre(i);
			}
		}
	}

	public void afficheLivreGenre(Egenre g) {
		for (int i = 0; i < limite; i++) {
			if (getGenre(i) == g) {
				afficheLivre(i);
			}
		}
	}
	
	public Vector tabIndicePar(String auteur) {
		Vector v = new Vector();
		for (int i =0; i < limite; i++) {
			if (getAuteur(i).equals(auteur)) {
				v.add(i);
				
			}
		}
		return v;
	}
	
	public void ajoutDisque(Disque d) {
		if (limite < taille) {
			catalogue[limite] = d;
			limite++;		
		} else {
			System.out.println("---------- catalogue plein ----------");
		}
	}
}
