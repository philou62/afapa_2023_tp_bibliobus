package bibliobus;

public enum Egenre {
	Policier(1), Litterature(1), LitteratureJeunesse(1), BandeDessinee(1), Documentaire(1), NonSpecifie(0),
	Classique(2), Jazz(2), MusiqueDuMonde(2), Rock(2), Pop(2), ChansonFrançaise(2);

	int support;

	 Egenre(int eg) {
		support = eg;
	}
	 
	 int getSupport() {
		 return support;
	 }
	

}
