package bibliobus;

import bibliobus.Egenre;

public class DemoLivre {
	public static void main(String[] args) {

		Livre monLivre = new Livre("Emile Zola", "La bête humaine", "moi", Egenre.Documentaire, 5);
		Livre tonLivre = new Livre("je suis une legende!", "fghfgh", "fgfdgfg", Egenre.BandeDessinee, 8);
		tonLivre.nouvelExemplaire();
		tonLivre.nouvelExemplaire();
		
		System.out.println(tonLivre);

		Bibliobus b1 = new Bibliobus("Bibli1", 400);
		Bibliobus b2 = new Bibliobus("Bibli2", 5400);

		b1.ajoutLivre("la saga", "moi", Egenre.BandeDessinee, "yoyo");
		b1.afficheLivre(0);

		b2.ajoutLivre("lo", "yaya", Egenre.LitteratureJeunesse, "grrrr");
		b2.ajoutLivre("kkkk", "nnnnnn", Egenre.Documentaire, "dddd");
		b2.afficheCatalogue();

		System.out.println(monLivre.estPresent());
		System.out.println(tonLivre);
		
		System.out.println("titre de mon livre => " + tonLivre.getTitre() + tonLivre.getGenre());

	}
}