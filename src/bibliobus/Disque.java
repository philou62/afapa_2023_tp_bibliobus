package bibliobus;

import java.util.*;

public class Disque extends Media {
	
	List<String> pistes = new ArrayList<String>();

	public Disque(String titre, String auteur, int nbExemplaire, List<String> p) {
		super(titre, auteur, nbExemplaire);
		this.pistes = p;
	}

	public List<String> getPistes() {
		return pistes;
	}

	public void setPistes(List<String> pistes) {
		this.pistes = pistes;
	}

	@Override
	public void afficher() {
		// TODO Auto-generated method stub
		
	}

	

}
