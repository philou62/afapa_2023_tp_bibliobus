package bibliobus;

public class Livre extends Media {

	protected String editeur;

	

	public Livre(String titre, String auteur, String editeur, int nbExemplaire) {
		super(titre, auteur, nbExemplaire);
		this.editeur = editeur;
		
	}

	public Livre(String titre, String auteur, String editeur, Egenre genre, int nbExemp) {
		super(titre, auteur, Egenre.NonSpecifie, nbExemp);
		this.editeur = editeur;
	}

	public String getEditeur() {
		return editeur;
	}
	
	public Livre nouvelEditeur1(String editeur) {
		Livre liv = new Livre(this.auteur, this.titre, editeur, this.genre, this.nbExemplaire);
		return liv;
	}

	// Verification de voir si le genre appartient a la liste Egenre pour un livre
	public boolean genreCorrect(Egenre g) {
		for (Egenre e : Egenre.values()) {
			if ((e == g)) {
				return true;
			}
		}
		return false;

	}

	// Verification de voir si le genre appartient a la liste Egenre pour un livre
	public boolean genreCorrect1(Egenre g) {
		for (Egenre e : Egenre.values()) {
			if ((e == g) && (e.getSupport() != 2)) {
				return true;
			}
		}	
		return false;	
	}
	
	public boolean genreCorrect(String ch) {
		Egenre g;
		try {
			g = Egenre.valueOf(ch);
		} catch (Exception ex) {
			System.out.println("Impossible de convertir");
			return false;
		}
		return true;
	}
	
	public Livre nouvelEditeur(String editeur) {
		Livre liv = new Livre(this.auteur, this.titre, this.editeur, this.genre, this.nbExemplaire);
		return liv;
	}
	
	
	public boolean equals(Livre l) {
		if ((this.titre == l.titre) & (this.auteur == l.auteur) & (this.editeur == l.editeur)) {
			return true;
		}
		return false;
	}

	public String toString() {
		return "Livre [titre=" + titre + ",auteur=" + auteur + ",editeur=" + editeur + ",nbExemp=" + nbExemplaire + "]";
	}

	@Override
	public void afficher() {
		// TODO Auto-generated method stub
		
	}
}