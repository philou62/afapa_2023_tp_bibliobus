package bibliobus;

import bibliobus.Egenre;

public class DemoBibliobus {
	
	public static void main(String[] args) {
		Bibliobus b1 = new Bibliobus("Veleda");
		Bibliobus b3 = new Bibliobus("b2");
		b1.ajoutLivre("Les misérables", "Victor Hugo", Egenre.NonSpecifie, "Eyrolls");
		b1.ajoutLivre("Enquêtes Impossible", "Pierre Bellemarre", Egenre.Documentaire, "Eyrolls");
		b1.ajoutLivre("Don Von-room", "Chanks", Egenre.Policier, "victor hugo");	
		b3.ajoutLivre("tyyt", "tyty", Egenre.BandeDessinee, "victor hugo");
		b1.ajoutLivre("lol", "lol", Egenre.Policier, "lol");

		b1.afficheCatalogue();
		// ****************************************//
		b1.afficheLivre(1);
		b1.afficheCatalogue();
		System.out.println("-------------------------------------------------------------------------------------------------------------------");
//		System.out.println(b1.nbreExemplairePar(Egenre.Policier));
		System.out.println("-------------------------------------------------------------------------------------------------------------------");
//		System.out.println(b1.nbreExemplairePar("Victor Hugo"));
		Bibliobus b88 = new Bibliobus("moi");
		b88.ajoutLivre("aaaaa", "aaaaa",Egenre.BandeDessinee, "aaaaa");
		b88.afficheLivre(0);
		System.out.println(b88.getAuteur(0));
	}
}
