package bibliobus;

public abstract class Media {

	// Variables
	protected String titre, auteur, nom;
	protected int nbExemplaire = 1;
	protected Egenre genre;
	
	// METHODE ABSTRAITE
	public abstract void afficher();
	
	public Media(String titre, String auteur, Egenre genre, int nbExemplaire) {
		this.titre = titre;
		this.auteur = auteur;
		this.nbExemplaire = nbExemplaire;
		this.genre = genre;
	}
	
	public Media(String titre, String auteur, int nbExemp) {
		this.titre = titre;
		this.auteur = auteur;
		this.nbExemplaire = nbExemp;
		this.genre = Egenre.NonSpecifie;
	}

	public int getNbExemp() {
		return nbExemplaire;
	}

	public void setNbExemp(int nbExemp) {
		this.nbExemplaire = nbExemp;
	}

	public Egenre getGenre() {
		return genre;
	}

	public void setGenre(Egenre genre) {
		this.genre = genre;
	}

	public String getTitre() {
		return titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void nouvelExemplaire() {
		nbExemplaire = nbExemplaire + 1;
	}

	public void nouvelExemplaire(int nb) {
		nbExemplaire = nbExemplaire + nb;
	}

	public void perteExemplaire() {
		if (nbExemplaire > 0) {
			nbExemplaire = nbExemplaire - 1;
		} else {
			System.err.println("Erreur : il n'y a pas d'exemplaires !");
		}
	}

	public boolean estPresent() {
		if (nbExemplaire > 0) {
			return true;
		}
		return false;
	}

	// Verification de voir si le genre appartient a la liste Egenre pour un livre
	public boolean genreCorrect(Egenre g) {
		for (Egenre e : Egenre.values()) {
			if (e == g) {
				return true;
			}
		}
		return false;

	}
	
	@Override
	public String toString() {
		return "Media [titre=" + titre + ", auteur=" + auteur +  ", genre=" + genre + ", nbExemplaire=" + nbExemplaire + ", genre=" + genre+ "]";
	}

}
